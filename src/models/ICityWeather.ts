export interface ICityWeather {
  id: string;
  name: string;
  country: string;
  coord: {
    lon: number;
    lat: number;
  };
  weather: {
    summary: {
      title: string;
      description: string;
      icon: string;
    };
    temperature: {
      actual: number;
      feelsLike: number;
      min: number;
      max: number;
    };
    clouds: {
      all: number;
      visibility: number;
      humidity: number;
    };
    wind: {
      speed: number;
      deg: number;
    };
  };
}

export interface ICityByNameWeather {
  getCityByName: ICityWeather;
}
