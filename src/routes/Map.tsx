import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";

export default function Map() {
  return (
    <Container maxWidth="lg" style={{ marginTop: 15, marginBottom: 15 }}>
      <Grid container spacing={2} justifyContent="center" alignItems="center">
        <Grid item md={10}>
          <Card>
            <CardHeader title="Map" />
            <CardContent>Map is not available now!</CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
