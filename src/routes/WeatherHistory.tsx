import { useEffect, useState } from "react";
import Container from "@mui/material/Container";
import { ApolloClient, InMemoryCache, useQuery } from "@apollo/client";
import Grid from "@mui/material/Grid";

import { getWeatherHistory } from "../api/API";
import { GQL_GetCityByName } from "../api/GraphQL";
import { IWeatherHistory } from "../models/IWeatherHistory";
import { ICityWeather, ICityByNameWeather } from "../models/ICityWeather";
import LoadingCard from "../components/LoadingCard";
import SummaryCard from "../components/SummaryCard";
import TempHistoryCard from "../components/TempHistoryCard";
import ChartCard from "../components/ChartCard";

const weatherClient = new ApolloClient({
  uri: "https://graphql-weather-api.herokuapp.com/",
  cache: new InMemoryCache()
});

interface IProps {
  defaultCity: string;
}

const WeatherHistory: React.FC<IProps> = ({ defaultCity }) => {
  // declare states
  const [cityName, setCityName] = useState<string>(defaultCity);
  const [result, setResult] = useState<ICityWeather>();
  const [pageLoading, setPageLoading] = useState(false);
  const [historyResult, setHistoryResult] = useState<IWeatherHistory[]>([]);

  // call gql query
  const { loading } = useQuery(GQL_GetCityByName, {
    client: weatherClient,
    variables: {
      name: cityName
    },
    onCompleted: (data: ICityByNameWeather) => {
      setResult(data.getCityByName);
    }
  });

  // get weather history by city name
  useEffect(() => {
    if (!result) return;
    (async () => {
      setPageLoading(true);
      const timestamp = Math.ceil(Date.now() / 1000) - 24 * 3600;
      const results = await getWeatherHistory(result.coord.lat, result.coord.lon, timestamp);
      setHistoryResult(results);
      setPageLoading(false);
    })();
  }, [result?.id]);

  const isChartReady = !pageLoading && historyResult.length > 0;

  return (
    <Container maxWidth="lg" style={{ marginTop: 15, marginBottom: 15 }}>
      <Grid container spacing={2} justifyContent="center" alignItems="center">
        {loading && (
          <Grid item md={10}>
            <LoadingCard />
          </Grid>
        )}
        {!loading && !!result && (
          <Grid item md={10}>
            <SummaryCard data={result} />
          </Grid>
        )}
        {isChartReady && (
          <Grid item md={10}>
            <TempHistoryCard records={historyResult} />
          </Grid>
        )}
        {isChartReady && (
          <Grid item md={10}>
            <ChartCard
              records={historyResult}
              cardTitle="Clouds"
              cardSubHeader="history over the last 24 hours"
              chartVAxisTitle="Clouds"
              chartVAxisData="clouds"
            />
          </Grid>
        )}
        {isChartReady && (
          <Grid item md={10}>
            <ChartCard
              records={historyResult}
              cardTitle="Humidity"
              cardSubHeader="history over the last 24 hours"
              chartVAxisTitle="Humidity"
              chartVAxisData="humidity"
            />
          </Grid>
        )}
        {isChartReady && (
          <Grid item md={10}>
            <ChartCard
              records={historyResult}
              cardTitle="Pressure"
              cardSubHeader="history over the last 24 hours"
              chartVAxisTitle="Pressure"
              chartVAxisData="pressure"
            />
          </Grid>
        )}
      </Grid>
    </Container>
  );
};
export default WeatherHistory;
