import * as React from 'react';
import { NavLink, Outlet } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";

export default function App() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <NavLink
            style={({ isActive }) => {
              return {
                color: isActive ? "#400147" : "",
                marginRight: "25px"
              };
            }}
            to="/weather-history">
            <Button color="inherit" variant="contained">
              Weather History
            </Button>
          </NavLink>

          <NavLink
            style={({ isActive }) => {
              return {
                color: isActive ? "#400147" : ""
              };
            }}
            to="/map">
            <Button color="inherit" variant="contained">
              Map
            </Button>
          </NavLink>
        </Toolbar>
      </AppBar>

      <Outlet />
    </Box>
  );
}
