import axios from "axios";
import { IWeatherHistory } from "../models/IWeatherHistory";

export const getWeatherHistory = async (lat: number, lon: number, dt: number): Promise<IWeatherHistory[]> => {
  return await axios
    .get<string>("https://api.openweathermap.org/data/2.5/onecall/timemachine", {
      params: {
        appid: "8863ca2f97cea17a004287f3a3a07e67",
        units: "metric",
        lat,
        lon,
        dt
      }
    })
    .then(response => {
      return JSON.parse(JSON.stringify(response.data)).hourly;
    })
    .catch(err => {
      console.log("Error on getWeatherHistory: ", err);
      throw new Error("Error on getWeatherHistory: " + err);
    });
};
