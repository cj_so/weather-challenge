import { Grid, CircularProgress } from "@mui/material";

const LoadingCard = () => {
  return (
    <Grid container>
      <Grid item md={12} alignItems="center" justifyContent="center" display="flex">
        <CircularProgress size={80} />
      </Grid>
    </Grid>
  );
};
export default LoadingCard;
