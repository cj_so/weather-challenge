import React from "react";
import { Chart } from "react-google-charts";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import { IWeatherHistory } from "../models/IWeatherHistory";

interface IProps {
  records: IWeatherHistory[];
}

const TempHistoryCard: React.FC<IProps> = ({ records }) => {
  return (
    <Card>
      <CardHeader title="Temperature" subheader="history over the last 24 hours" />
      <CardContent>
        <Chart
          chartType="LineChart"
          height={"300px"}
          loader={<span>Loading...</span>}
          data={[
            ["x", "feels like", "actual"],
            ...records.map(record => {
              return [new Date(record.dt * 1000).getHours().toString(), record.feels_like, record.temp];
            })
          ]}
          options={{
            hAxis: {
              title: "Hour of day"
            },
            vAxis: {
              title: "temerature"
            }
          }}
          rootProps={{ "data-testid": "1" }}
        />
      </CardContent>
    </Card>
  );
};

export default TempHistoryCard;
