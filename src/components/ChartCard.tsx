import React from "react";
import { Chart } from "react-google-charts";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import { IWeatherHistory } from "../models/IWeatherHistory";

interface IProps {
  records: IWeatherHistory[];
  cardTitle: string;
  cardSubHeader: string;
  chartVAxisTitle: string;
  chartVAxisData: string;
}

const ChartCard: React.FC<IProps> = ({ records, cardTitle, cardSubHeader, chartVAxisTitle, chartVAxisData }) => {
  return (
    <Card>
      <CardHeader title={cardTitle} subheader={cardSubHeader} />
      <CardContent>
        <Chart
          chartType="LineChart"
          height={"300px"}
          loader={<span>Loading...</span>}
          data={[
            ["x", "%"],
            ...records.map(record => [
              new Date(record.dt * 1000).getHours().toString(),
              new Map(Object.entries(record)).get(chartVAxisData)
            ])
          ]}
          options={{
            hAxis: {
              title: "Hour of day"
            },
            vAxis: {
              title: chartVAxisTitle
            }
          }}
        />
      </CardContent>
    </Card>
  );
};

export default ChartCard;
