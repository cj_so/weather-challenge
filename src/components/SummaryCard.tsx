import * as React from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Grid, Tooltip } from "@mui/material";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import InsertEmoticonIcon from "@mui/icons-material/InsertEmoticon";
import DeviceThermostatIcon from "@mui/icons-material/DeviceThermostat";
import FastForward from "@mui/icons-material/FastForward";
import EmojiFlags from "@mui/icons-material/EmojiFlags";
import Water from "@mui/icons-material/Water";
import Visibility from "@mui/icons-material/Visibility";
import { ICityWeather } from "../models/ICityWeather";

interface IProps {
  data: ICityWeather;
}

const SummaryCard: React.FC<IProps> = ({ data }) => {
  return (
    <Card>
      <CardHeader title={data.name} subheader={data.country} />
      <CardContent>
        <Grid container>
          <Grid item md={4}>
            <Typography variant="h5" color="text.secondary">
              {data.weather.summary.title}
            </Typography>
            <Typography variant="subtitle1" color="text.secondary">
              {data.weather.summary.description}
            </Typography>
          </Grid>
          <Grid item md={2}>
            <Tooltip title="actual temperature">
              <Typography variant="subtitle1" color="text.secondary">
                <DeviceThermostatIcon style={{ verticalAlign: "middle", width: 20, marginRight: 5 }} />
                {data.weather.temperature.actual} °C
              </Typography>
            </Tooltip>
            <Tooltip title="feels like temperature">
              <Typography variant="subtitle1" color="text.secondary">
                <InsertEmoticonIcon style={{ verticalAlign: "middle", width: 20, marginRight: 5 }} />
                {data.weather.temperature.feelsLike} °C
              </Typography>
            </Tooltip>
          </Grid>
          <Grid item md={2}>
            <Tooltip title="maximum temperature">
              <Typography variant="subtitle1" color="text.secondary">
                <ArrowDropUpIcon style={{ verticalAlign: "bottom" }} />
                {data.weather.temperature.max} °C
              </Typography>
            </Tooltip>
            <Tooltip title="minimum temperature">
              <Typography variant="subtitle1" color="text.secondary">
                <ArrowDropDownIcon style={{ verticalAlign: "top" }} />
                {data.weather.temperature.min} °C
              </Typography>
            </Tooltip>
          </Grid>
          <Grid item md={2}>
            <Tooltip title="wind degree">
              <Typography variant="subtitle1" color="text.secondary">
                <EmojiFlags style={{ verticalAlign: "middle", width: 20, marginRight: 5 }} />
                {data.weather.wind.deg} °
              </Typography>
            </Tooltip>
            <Tooltip title="wind speed">
              <Typography variant="subtitle1" color="text.secondary">
                <FastForward style={{ verticalAlign: "middle", width: 20, marginRight: 5 }} />
                {data.weather.wind.speed} Km/h
              </Typography>
            </Tooltip>
          </Grid>
          <Grid item md={2}>
            <Tooltip title="humidity">
              <Typography variant="subtitle1" color="text.secondary">
                <Water style={{ verticalAlign: "middle", width: 20, marginRight: 5 }} />
                {data.weather.clouds.humidity} %
              </Typography>
            </Tooltip>
            <Tooltip title="visibility">
              <Typography variant="subtitle1" color="text.secondary">
                <Visibility style={{ verticalAlign: "middle", width: 20, marginRight: 5 }} />
                {data.weather.clouds.visibility} m
              </Typography>
            </Tooltip>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default SummaryCard;
