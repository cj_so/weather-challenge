# Weather History Challenge

## Live Demo

To see a live demo version of the master branch you can check this [link](https://quizzical-fermat-8f5f2b.netlify.app/).


##  Description
This is a react project with the help of Apollo Client for Graphql developing and Material UI(mui) for designing and styling components.  

In this project I utilized these APIs for fetching data:  

- [OpenWeather One Call API](https://openweathermap.org/api/one-call-api) get city information (`REST`)
- [graphql-weather-api.herokuapp.com](https://graphql-weather-api.herokuapp.com) get weather history of a city (`GraphQL`)


## Requirements

- NodeJS
- NPM for package manager


## How to use

Download the project [or clone the repo](https://bitbucket.org/cj_so/weather-challenge/):

Install it and run:

```sh
npm install
npm start
```
